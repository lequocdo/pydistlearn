from datetime import datetime
import signal
import sys
import os
import argparse

import redis
from ujson import loads, dumps

from utils import DATETIME_FORMAT, class_import
from keys import WORKERS_KEY, JOB_TYPES_KEY, WORKER_INPUT_KEY, WORKER_WORKING_STATUS_KEY, LAST_PING_KEY


class Worker:
    def __init__(self, job_name, worker_key, redis_host, redis_status_port, redis_model_port, redis_pass):
        self.job_name = job_name
        self.status = "working"
        self.worker_key = worker_key
        print "Worker ID:", self.worker_key
        self.full_name = 'jobs::%s::worker::%s' % (self.job_name, self.worker_key)
        self.timeout = None
        self.input_queue = WORKER_INPUT_KEY % (self.job_name, "worker::" + self.worker_key)
        self.working_queue = WORKER_WORKING_STATUS_KEY % (self.full_name, self.status)
        self.redis_status = redis.StrictRedis(host=redis_host, port=redis_status_port,  password=redis_pass)
        self.redis_model = redis.StrictRedis(host=redis_host, port=redis_model_port,  password=redis_pass)

        self.initialize()

    def handle_signal(self, number, stack):
        self.unregister()
        sys.exit(number)

    def initialize(self):
        signal.signal(signal.SIGTERM, self.handle_signal)
        self.ping()
        input_data = self.redis_status.rpop(self.working_queue)
        if input_data:
            data_input = loads(data_input)
            self.assign_task(data_input)

    def worker_process(self):
        raise NotImplementedError()

    def run(self):
        try:
            while True:
                self.ping()
                data_input = self.redis_status.get(self.input_queue)
                if data_input:
                    self.redis_status.delete(WORKER_WORKING_STATUS_KEY % (self.full_name, "done"))
                    self.assign_task(data_input)
                    #self.redis_status.delete(self.input_queue)
        finally:
            self.unregister()

    def unregister(self):
        self.redis_status.srem(WORKERS_KEY, self.full_name)
        self.redis_status.delete(LAST_PING_KEY % self.full_name)
        self.redis_status.delete(WORKER_WORKING_STATUS_KEY % (self.full_name, "done"))

    def ping(self):
        self.redis_status.delete(WORKER_WORKING_STATUS_KEY % (self.full_name, "working"))
        self.redis_status.sadd(JOB_TYPES_KEY, self.job_name)
        self.redis_status.sadd(WORKERS_KEY, self.full_name)
        self.redis_status.set(LAST_PING_KEY % self.full_name, datetime.now().strftime(DATETIME_FORMAT))

    def assign_task(self, data_input):
        result = self.worker_process(data_input)
        self.status = "done"
        self.redis_status.set('%s::status::%s' % (self.full_name, self.status), str(result))
        self.redis_status.delete(self.working_queue)
     


def main(arguments=None):
    if not arguments:
        arguments = sys.argv[1:]

    parser = argparse.ArgumentParser(description='runs workers to connect parameter servers')
    parser.add_argument('--redis-host', type=str, default='0.0.0.0', help='the ip of parameter server')
    parser.add_argument('--redis-status-port', type=int, default=6868, help='the port for montioring status of workers')
    parser.add_argument('--redis-model-port', type=int, default=8686, help='the port for sharing model between workers')
    parser.add_argument('--redis-pass', type=str, default='', help='the password that worker will use to connect to parameter servers')
    parser.add_argument('--worker-key', type=str, help='the unique identifier for this worker', required=True)
    parser.add_argument('--worker-class', type=str, help='the fullname of the class that this worker will run', required=True)

    args = parser.parse_args(arguments)

    if not args.worker_key:
        raise RuntimeError('The --worker_key argument is required.')

    try:
        pyclass = class_import(args.worker_class)
    except Exception, err:
        print "Could not import the specified %s class. Error: %s" % (args.worker_class, err)
        raise

    worker = pyclass(pyclass.job_name, args.worker_key, redis_host=args.redis_host, redis_status_port=args.redis_status_port, redis_model_port=args.redis_model_port, redis_pass=args.redis_pass)
    try:
        worker.run()
    except KeyboardInterrupt:
        print
        print "--  Worker closed by user interruption --"


if __name__ == '__main__':
    main(sys.argv[1:])
