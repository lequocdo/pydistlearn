#!/usr/bin/python
# -*- coding: utf-8 -*-

from flask import Flask, render_template, g, redirect, url_for
from ujson import loads

#from r3.version import __version__
from utils import flush_dead_workers
from keys import WORKERS_KEY, JOB_TYPES_KEY, JOB_TYPE_KEY, LAST_PING_KEY, WORKER_ERROR_KEY, WORKER_KEY, JOB_TYPES_ERRORS_KEY, ALL_KEYS, PROCESSED, PROCESSED_FAILED

app = Flask(__name__)

def server_context():
    return {
        'Pyparameterserver_service_status': 'running',
        'Pyparameterserver_version': 1.0#__version__
    }

@app.before_request
def before_request():
    g.config = app.config
    g.server = server_context()
    g.job_names = app.db.connection.smembers(JOB_TYPES_KEY)
    g.jobs = get_all_jobs(g.job_names)
    g.workers = get_workers()

def get_workers():
    all_workers = app.db.connection.smembers(WORKERS_KEY)
    workers_status = {}
    for worker in all_workers:
        key = WORKER_KEY % worker
        working = app.db.connection.lrange(key, 0, -1)
        if not working:
            workers_status[worker] = None
        else:
            workers_status[worker] = loads(working[0])

    return workers_status

def get_all_jobs(all_job_names):
    all_jobs = {}
    for job_name in all_job_names:
        jobs = app.db.connection.smembers(JOB_TYPE_KEY % job_name)
        all_jobs[job_name] = []
        if jobs:
            all_jobs[job_name] = jobs

    return all_jobs

def get_errors():
    errors = []
    for job_name in g.job_names:
        errors = [loads(item) for key, item in app.db.connection.hgetall(WORKER_ERROR_KEY % job_name).iteritems()]

    return errors

@app.route("/")
def index():
    error_queues = app.db.connection.keys(JOB_TYPES_ERRORS_KEY)

    has_errors = False
    for queue in error_queues:
        if app.db.connection.hlen(queue) > 0:
            has_errors = True

    flush_dead_workers(app.db.connection, WORKERS_KEY, LAST_PING_KEY)

    return render_template('index.html')

@app.route("/workers")
def workers():
    flush_dead_workers(app.db.connection, WORKERS_KEY, LAST_PING_KEY)
    return render_template('workers.html')

@app.route("/jobs")
def job_names():
    return render_template('jobs.html')

@app.route("/stats")
def stats():
    info = app.db.connection.info()
    key_names = app.db.connection.keys(ALL_KEYS)

    keys = []
    for key in key_names:
        key_type = app.db.connection.type(key)

        if key_type == 'list':
            size = app.db.connection.llen(key)
        elif key_type == 'set':
            size = app.db.connection.scard(key)
        else:
            size = 1

        keys.append({
            'name': key,
            'size': size,
            'type': key_type
        })

    processed = app.db.connection.get(PROCESSED)

    return render_template('stats.html', info=info, keys=keys, processed=processed)

@app.route("/stats/keys/<key>")
def key(key):
    key_type = app.db.connection.type(key)

    if key_type == 'list':
        value = app.db.connection.lrange(key, 0, -1)
        multi = True
    elif key_type == 'set':
        value = app.db.connection.smembers(key)
        multi = True
    else:
        value = app.db.connection.get(key)
        multi = False

    return render_template('show_key.html', key=key, multi=multi, value=value)

@app.route("/stats/keys/<key>/delete")
def delete_key(key):
    app.db.connection.delete(key)
    return redirect(url_for('stats'))
 
