from datetime import datetime

DATETIME_FORMAT = '%Y-%m-%d %H:%M:%S'
TIMEOUT = 15

def real_import(name):
    if '.'  in name:
        return reduce(getattr, name.split('.')[1:], __import__(name))
    return __import__(name)


def flush_dead_workers(redis, workers_key, ping_key):
    workers = redis.smembers(workers_key)
    for worker in workers:
        last_ping = redis.get(ping_key % worker)
        if last_ping:
            now = datetime.now()
            last_ping = datetime.strptime(last_ping, DATETIME_FORMAT)
            if ((now - last_ping).seconds > TIMEOUT):
                redis.srem(workers_key, worker)
                redis.delete(ping_key % worker)


def class_import(fullname):
    #print fullname
    if not '.' in fullname:
        return __import__(fullname)

    name_parts = fullname.split('.')
    class_name = name_parts[-1]
    module_parts = name_parts[:-1]
    module = reduce(getattr, module_parts[1:], __import__('.'.join(module_parts)))
    pyclass = getattr(module, class_name)
    return pyclass


