#!/usr/bin/python
# -*- coding: utf-8 -*-

import sys
import os
from os.path import abspath, isabs, join
import argparse
import pydistlearn
from pydistlearn.web.app import app
from pydistlearn.web.extensions import RedisDB

def main(arguments=None):
    '''Runs pyparameterserver web app with the specified arguments.'''

    parser = argparse.ArgumentParser(description='runs the web that helps in monitoring pyparameterserver usage')
    parser.add_argument('-b', '--bind', type=str, default='0.0.0.0', help='the ip that pyparameterserver will bind to')
    parser.add_argument('-p', '--port', type=int, default=8888, help='the redis port to get status of workers ')
    parser.add_argument('--redis-host', type=str, default='0.0.0.0', help='the ip that pyparameterserver will use to connect to redis')
    parser.add_argument('--redis-port', type=int, default=6379, help='the port that pyparameterserver will use to connect to redis')
    parser.add_argument('--redis-pass', type=str, default='', help='the password that pyparameterserver will use to connect to redis')
    parser.add_argument('-c', '--config-file', type=str, default='', help='the configuration file that pyparameterserver will use')

    args = parser.parse_args(arguments)


    if args.config_file:
        config_path = args.config_file
        if not isabs(args.config_file):
            config_path = abspath(join(os.curdir, args.config_file))

        app.config.from_pyfile(config_path, silent=False)
    else:
        app.config.from_object('web.config')

    app.db = RedisDB(app)
    try:
        app.run(host=app.config['WEB_HOST'], port=app.config['WEB_PORT'])
    except KeyboardInterrupt:
        print
        print "-- pyparameterserver web app closed by user interruption --"

if __name__ == "__main__":
    main(sys.argv[1:])
