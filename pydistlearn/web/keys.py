ALL_KEYS = '*'

# WORKER KEYS
WORKERS_KEY = 'workers'
WORKER_KEY = '%s'
WORKER_INPUT_KEY = 'jobs::%s::%s::input'
WORKER_OUTPUT_KEY = 'jobs::%s::%s::output'
WORKER_ERROR_KEY = 'jobs::%s::errors'
WORKER_WORKING_STATUS_KEY = '%s::status::%s'
LAST_PING_KEY = '%s::last-ping'

# JOB TYPES KEYS
JOB_TYPES_KEY = 'jobs-name'
JOB_TYPES_ERRORS_KEY = 'jobs::*::errors'
JOB_TYPE_KEY = '%s'

# STATS KEYS
PROCESSED = 'stats::processed'
PROCESSED_SUCCESS = 'stats::processed::success'
PROCESSED_FAILED = 'stats::processed::fail'

