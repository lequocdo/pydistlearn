import sys
import json
import ast
import redis
import time
from os.path import abspath, dirname, join
from utils import class_import
import argparse


class ParameterServer:
    def __init__(self, job_name, redis_host, redis_status_port, redis_model_port, redis_pass, file_name):
        self.job_name = job_name
        self.redis_status = redis.StrictRedis(host=redis_host, port=redis_status_port,  password=redis_pass)
        self.redis_model = redis.StrictRedis(host=redis_host, port=redis_model_port,  password=redis_pass)
        self.file_name = file_name
        print "\n############# Parameter Server Service - TUD #############"


    def load_data_loop(self):
        done_workers_list = self.redis_status.keys("jobs::" + self.job_name + "::worker::*::status::done")
        workers_list = self.redis_status.keys("jobs::" + self.job_name + "::worker::*::last-ping")    
        #print len(done_workers_list)
        if (len(done_workers_list) == len(workers_list))and(workers_list is not None):
            print "Enter the learning loop."
            #print self.labels[data.index(sv)], sv
            shared_data = self.redis_model.keys("*")
            labels = []
            if shared_data:
                for key in shared_data:
                    labels.append(int(self.redis_model.get(key)))
                #print labels
                shared_input = str(ast.literal_eval(json.dumps(shared_data))) + "::" + str(json.dumps(labels))
                for worker in workers_list:
                    worker_input_queue = worker.split("last-ping")[0] + "input"
                    #print worker_input_queue
                    self.redis_status.set(worker_input_queue, shared_input)
                    self.redis_status.delete("jobs::" + self.job_name + "::worker::" + worker + "::status::done")
            return 0

        else:

            return -1


    def process(self, filename):
        with open(abspath(join(dirname(__file__), filename))) as f:
            contents = f.readlines()

        return [line for line in contents]
   

    def convert_data(self, lines):
        labels = []
        data = []
        for line in lines:
            vector = line.split(" ")
            vt = vector[1]
            for i in range(2, len(vector)):
                if "\n" not in vector[i]:
                    vt = vt + ", " + vector[i]
            labels.append(int(vector[0]))
            data.append(ast.literal_eval("{" + vt + "}"))

        return data, labels


    def load_data_to_workers(self, filename):
        lines = self.process(filename)
        data, labels = self.convert_data(lines)
        #print labels
        workers_list = self.redis_status.keys("jobs::" + self.job_name + "::worker::*::last-ping")
        if workers_list is not None:
            for worker in workers_list:
                worker_input_queue = worker.split("last-ping")[0] + "input"
                input_data = str(data) + "::" + str(json.dumps(labels))
                self.redis_status.set(worker_input_queue, input_data)

    def run(self):
        try:
            #while True:
            self.server_process(self.file_name)
            #self.redis_model.flushall()
            #self.redis_status.flushall()
        finally:
            print "Stop running Parameter server process."
            #Delete all data in the database
            self.redis_model.flushall()
            self.redis_status.flushall()


    def server_process(self):
        raise NotImplementedError()


def main(arguments=None):
    if not arguments:
        arguments = sys.argv[1:]

    parser = argparse.ArgumentParser(description='runs workers to connect parameter servers')
    parser.add_argument('--redis-host', type=str, default='0.0.0.0', help='the ip of parameter server')
    parser.add_argument('--redis-status-port', type=int, default=6868, help='the port for montioring status of workers')
    parser.add_argument('--redis-model-port', type=int, default=8686, help='the port for sharing model between workers')
    parser.add_argument('--redis-pass', type=str, default='', help='the password that worker will use to connect to parameter servers')
    parser.add_argument('--train-data-file', type=str, default="train-data.txt", help='the tranining data input file') 
    parser.add_argument('--server-class', type=str, help='the fullname of the class that this server will run', required=True)

    args = parser.parse_args(arguments)

    try:
        pyclass = class_import(args.server_class)
    except Exception, err:
        print "Could not import the specified %s class. Error: %s" % (args.server_class, err)
        raise


    #print pyclass.job_name
    #print args.redis_host 
    server = pyclass(pyclass.job_name, redis_host=args.redis_host, redis_status_port=args.redis_status_port, redis_model_port=args.redis_model_port, redis_pass=args.redis_pass, file_name=args.train_data_file)
    try:
        server.run()
    except KeyboardInterrupt:
        print
        print "--  Server closed by user interruption --"


if __name__ == '__main__':
    main(sys.argv[1:])
