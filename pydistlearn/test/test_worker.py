import sys
from worker import Worker

sys.path.append('/home/ubuntu/libsvm/python')
from svm import *
from svmutil import *

import json
import ast
import redis
import time

class SVMTrainWorker(Worker):
    job_name = 'svm-training'
    loop = 0
    redis_local = redis.Redis("127.0.0.1", 3737)
    def worker_process(self, lines):
        print "Start processing..."

	    #Get data from server nodes
        input_data = lines.split("::")
        data = ast.literal_eval(input_data[0])   
        try:    
            data = [ast.literal_eval(item) for item in data]
        except:
            data = ast.literal_eval(input_data[0])

        #Labels from server nodes
        labels = json.loads(input_data[1])
        labels = [int(item) for item in labels]	


        #Get data from local cache
        local_cache =self.redis_local.get(str(self.worker_key)) #update to local cache
        if local_cache is not None:
            cache_data = local_cache.split("::")
            local_data = ast.literal_eval(cache_data[0])   
            try:    
                local_data = [ast.literal_eval(item) for item in local_data]
            except:
                local_data = ast.literal_eval(cache_data[0])
	        print len(data)
	        data = data + local_data
            print len(data)	
            #Labels from local cache
            local_labels = json.loads(cache_data[1])
            local_labels = [int(item) for item in local_labels]
            print len(labels)
            labels = labels + local_labels
            print "Lablel" , len(labels)
        
        if self.loop == 0:
            self.redis_local.set(str(self.worker_key), lines) #update to local cache        

        #Training
        if self.loop < 3:
            result = self.train_model(data, labels)
        else:
            print "Reach the iteration limit number"
            self.redis_status.set("Status", "Done!")
            return "Done!"

        return list(result)


    def train_model(self, data, labels):
        rmodel = self.redis_model
        study_data = svm_problem(list(labels), list(data))
        print labels
        print "Loop:", self.loop
        #print data
        #params = svm_parameter('-t 1 -c 3')
        params = svm_parameter('-t 0')
        model = svm_train(study_data, params)   
        svm_save_model('./loop'+ str(self.loop) + "worker" + str(self.worker_key) + '.model', model)
        self.loop += 1 

        for sv in model.get_SV():
            del sv[-1] 
            key = str(sv)
            value = labels[data.index(sv)]
            rmodel.set(key, value)

        yield "done", self.loop
