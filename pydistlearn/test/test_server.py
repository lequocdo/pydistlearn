#!/usr/bin/python
import sys
from parameterserver import ParameterServer
import json
import ast
import redis
import time

class SVMTrainServer(ParameterServer):
    job_name = "svm-training"

    def server_process(self, file_name):
        ok = self.load_data_to_workers(file_name)
        #self.load_data_loop()
        #ok = 1
        if ok == -1:
            print "\nNo worker is available. Please assign workers for processing."

        else:
            print "Loaded from file"
            #print self.load_data_loop()
            loop = 0
            while 1==1:
                if (self.load_data_loop() == 0):
                    print "Finished interation: ", loop
                done = self.redis_status.get("Status")
                if done == "Done!":
                    return 0
                #print "Running...", loop
                loop += 1

    def load_data_to_workers(self, file_name):
        workers = self.redis_status.keys("jobs::" + self.job_name + "::worker::*::last-ping")
        print len(workers)
        if len(workers) > 0:
            self.split_training_data(file_name, len(workers))
            name = 0
            for worker in workers:
                worker_key = int(worker.split("::")[3])
                splitted_file = "trainingdata/" + str(name) + ".txt"
                self.load_data_to_worker(splitted_file, worker_key)
                name += 1

            return 0
        else:

            return -1


    def load_data_to_worker(self, file_name, worker_key):
        lines = self.process(file_name)
        data, labels = self.convert_data(lines)
        worker = self.redis_status.keys("jobs::" + self.job_name + "::worker::" + str(worker_key)  + "::last-ping")
        #print worker, len(worker)
        if len(worker) > 0:
            worker_input_queue = worker[0].split("last-ping")[0] + "input"
            input_data = str(data) + "::" + str(json.dumps(labels))
            #print input_data
            #print worker_input_queue
            set_result = self.redis_status.set(worker_input_queue, input_data)
            #print set_result

            return 0

        else:

            return -1

    def split_training_data(self, file_name, num_workers):
        fr = open(file_name,"rb")
        for i, line in enumerate(fr): 
            print i
            fw = open("server/trainingdata/" + str(i%num_workers) + ".txt","a+")
            fw.write(line)
        fw.close()
        fr.close()


#if __name__=="__main__":
#    redis_model = redis.Redis("141.76.44.159", 8686)
#    redis_status = redis.Redis("141.76.44.159", 6868)
#    job_name = "svm-training"
#    filedata = "heart_scale"
#    server = SVMTrainServer(redis_model, redis_status, job_name)
#    ok = server.load_data_to_workers(filedata)
#    if ok == -1:
#        print "\nNo worker is available. Please assign workers for processing."


#    else:
#        loop = 0
#        while loop < 3:
#            if (server.load_data_loop() == 0):
#                print "Finished interation: ", loop
#                loop += 1
