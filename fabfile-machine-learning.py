#######################################################
#################### 20/06/2014 #######################
## setup Hadoop, Cassandra, Nutch: $fab setupCluster ##
## run Hadoop, Cassandra: $fab startCluster          ##
## run Nutch: $fab runNutch                          ##
## refresh Cluster: $fab refreshCluster              ##
#######################################################
##################### Version 1 #######################
######## Le Quoc Do - SE Group TU Dresden #############


from fabric.api import *
import fabric.contrib.files
import time
import logging
import os
from fabric.contrib.files import append

#Disable annoyting log messages.
logging.basicConfig(level=logging.ERROR)

#This makes the paramiko logger less verbose
para_log=logging.getLogger('paramiko.transport')
para_log.setLevel(logging.ERROR)

env.keepalive = True


########INPUT PARAMETERS###############
env.roledefs = {
    'servers': ['webgraph'],
    'workers': ['crawler0', 'crawler1'],
    #'workers': ['crawler0', 'crawler1', 'crawler2', 'crawler3', 'crawler4', 'rcaching', 'scaching'],
}



redis_status_host = '80.156.73.101'

env.user='ubuntu'
env.key_filename = '/home/ubuntu/.ssh/id_rsa'
#env.user='dlequoc'
mlhome = '/home/ubuntu/machinelearning/'


urlPydistlearn = 'https://lequocdo:lqd@123a@bitbucket.org/lequocdo/pydistlearn/'
urlSVMlib= 'https://github.com/cjlin1/libsvm.git'
numCores = 4

#####KEY MANAGEMENT####
def read_key_file(key_file):
    key_file = os.path.expanduser(key_file)
    if not key_file.endswith('pub'):
        raise RuntimeWarning('Trying to push non-public part of key pair')
    with open(key_file) as f:
        return f.read()
 

######INSTALL NUTCH HADOOP CASSANDRA#####

@roles('servers', 'workers')
def requirement():
    #sudo('sudo apt-get update')
    #run('echo "yes"|sudo apt-get install ant')
    #run('echo "yes"|sudo apt-get install openjdk-7-jdk')
    run('echo "Y" |sudo pip install ujson')

@roles('servers', 'workers')
def download():
    run('cd ' + mlhome + ' && git clone ' + urlPydistlearn + '&& git clone ' + urlSVMlib)
    run('cd ' + mlhome + 'libsvm && make lib')


@roles('workers')
def cleanModel():
    run('rm -rf ' + mlhome + '*.model')

@roles('servers')
def cleanData():
    run('rm -rf ' + mlhome + 'pydistlearn/pydistlearn/server/trainingdata/*.txt')
    run('python ' + mlhome + 'pydistlearn/flush.py')


@serial
def cleanCluster():
    execute(cleanModel)
    execute(cleanData)


@roles('workers')
def runWorker():
    worker_id = run('/bin/hostname -i') 
    for i in range(int(numCore)):
	cmd = 'cd ' + mlhome + 'pydistlearn/pydistlearn/ && python worker/worker.py  --worker-key=' + str(worker_id) + "_" + str(i) +  "--redis-status-port=6868 --redis-host=" + redis_status_host +  '--worker-class="pydistlearn.test.test_worker.SVMTrainWorker"'	
        run(cmd)
    
@roles('masters')
def runWeb():
    cmd =  'cd ' + mlhome + 'pydistlearn/pydistlearn/ && python web/server.py'


@roles('masters', 'slaves')
def stopCluster():
    run('pkill -9 python')

@serial
def runexperiment(): 
    execute(cleanCluster)
    execute(runWeb)
    execute(runWorker)
    execute(stopCluster)
