from setuptools import setup

#Read in the README for the long description on PyPI
def long_description():
    with open('README.md', 'r') as f:
        readme = unicode(f.read())
    return readme

setup(name='pydistlearn',
      version='0.1',
      description='module for implemeting distribtued machine learning algorithms in Python',
      long_description=long_description(),
      url='https://bitbucket.org/lequocdo/pydistlearn/',
      author='Do Le Quoc',
      author_email='lequocdo@gmail.com',
      license='MIT License',
      packages=['pydistlearn', 'pydistlearn.server', 'pydistlearn.worker',
          'pydistlearn.web', 'pydistlearn.test'],
      classifiers=[
          'Programming Language :: Python :: 2.7',],
      zip_safe=False)

